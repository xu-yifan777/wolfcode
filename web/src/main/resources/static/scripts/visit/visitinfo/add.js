layui.use(['form', 'layer','laydate'], function () {
    var form = layui.form,
        layer = layui.layer,
        laydate = layui.laydate,
        $ = layui.jquery;

    laydate.render({
        elem: '#visitData'
    });
    form.on('submit(Add-filter)', function (data) {
        $.ajax({
            url: web.rootPath() + "visitinfo/save",
            type: "post",
            contentType: "application/json",
            data: JSON.stringify(data.field),
            dataType: 'json',
            traditional: true,
            success: function (data) {
                layer.msg("操作成功", {
                    icon: 1,
                    success: function () {
                        reloadTb("Save-frame", "#SearchBtn");
                    }
                });
            },
            error: function (e) {
                layer.msg(e.responseJSON.message, {icon: 2});
            }

        });
        return false;
    });
    form.on('select(customerSelect)',function (data){
        console.log(data);
        $.ajax({
            url: web.rootPath() + "linkman/listByCustomerId?custId=" + data.value,
            type: "post",
            contentType: "application/json",
            dataType: 'json',
            traditional: true,
            success: function (data) {
                console.log(data)
                $("#linkman").empty();
                var optionHtml = '<option value="">请选择</option>'
                if(data.data.length>0){
                    data.data.forEach(item=>{
                        optionHtml+=`<option value="${item.id}">${item.linkman}</option>`
                    })
                }
                $("#linkman").html(optionHtml);

                // 渲染表单
                form.render('select','component-form-element')
            },
            error: function (e) {
                if(e.responseJSON.errCode === 1003){
                    layer.msg(e.responseJSON.data.toString(), {icon: 2})
                }else{
                    layer.msg(e.responseJSON.message, {icon: 2});
                }
            }

        });
        return false;
    })
});
