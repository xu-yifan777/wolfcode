package cn.wolfcode.web.modules.appdemo.service;

import cn.wolfcode.web.modules.appdemo.entity.Appdemo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xyf
 * @since 2023-06-25
 */
public interface IAppdemoService extends IService<Appdemo> {

}
