package cn.wolfcode.web.modules.custcontract.mapper;

import cn.wolfcode.web.modules.custcontract.entity.TbContract;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 合同信息 Mapper 接口
 * </p>
 *
 * @author xyf
 * @since 2023-06-29
 */
public interface TbContractMapper extends BaseMapper<TbContract> {

}
