package cn.wolfcode.web.modules.custinfo.UniqueAnnotation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = { UniqueValidator.class })

public @interface Unique {
    String message() default "企业名称已存在";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
