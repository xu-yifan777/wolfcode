package cn.wolfcode.web.modules.orderinfo.controller;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.wolfcode.web.commons.utils.PoiExportHelper;
import cn.wolfcode.web.modules.custinfo.entity.TbCustomer;
import cn.wolfcode.web.modules.custinfo.service.ITbCustomerService;
import cn.wolfcode.web.modules.linkman.entity.TbCustLinkman;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.wolfcode.web.modules.log.LogModules;

import cn.wolfcode.web.modules.orderinfo.entity.TbOrderInfo;
import cn.wolfcode.web.modules.orderinfo.service.ITbOrderInfoService;

import cn.wolfcode.web.commons.entity.LayuiPage;
import cn.wolfcode.web.commons.utils.LayuiTools;
import cn.wolfcode.web.commons.utils.SystemCheckUtils;
import cn.wolfcode.web.modules.BaseController;
import link.ahsj.core.annotations.AddGroup;
import link.ahsj.core.annotations.SameUrlData;
import link.ahsj.core.annotations.SysLog;
import link.ahsj.core.annotations.UpdateGroup;
import link.ahsj.core.entitys.ApiModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.Now;
import org.apache.poi.ss.usermodel.Workbook;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author xyf
 * @since 2023-06-29
 */
@Controller
@RequestMapping("orderinfo")
public class TbOrderInfoController extends BaseController {

    @Autowired
    private ITbOrderInfoService entityService;

    @Autowired
    private ITbCustomerService customerService;
    private static final String LogModule = "TbOrderInfo";



    @RequestMapping("confirm/{id}")
    public ResponseEntity<ApiModel> confirm(@PathVariable("id") String id){
//        entityService.lambdaUpdate().eq(TbOrderInfo::getCustId,id).set(!StringUtils.isEmpty(id),TbOrderInfo::getReceiveTime,LocalDateTime.now());
        entityService.lambdaUpdate().set(TbOrderInfo::getReceiveTime,LocalDateTime.now()).eq(TbOrderInfo::getId,id).update();
        return ResponseEntity.ok(ApiModel.ok());
    }


    /**
     * 导出
     */

    @RequestMapping("export")
    public void export(String parameterName, String custId, HttpServletResponse response) {

        // 把数据导出到表格当中
        List<TbOrderInfo> list = entityService.lambdaQuery()
                .eq(!StringUtils.isEmpty(custId), TbOrderInfo::getCustId, custId)
                .list();

        // 执行文件导出 准备工作
        ExportParams exportParams = new ExportParams();
        /**
         * 参数一：样式
         * 参数二：导出的实体类的字节码
         * 参数三：数据列表
         */
        Workbook workbook = ExcelExportUtil.exportExcel(exportParams, TbOrderInfo.class, list);

        // 导出
        try {
            PoiExportHelper.exportExcel(response,"订单管理\n"+LocalDateTime.now(),workbook);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }


    @GetMapping("/list.html")
    public ModelAndView list(ModelAndView mv) {
        List<TbCustomer> list = customerService.list();
        mv.addObject("customs", list);
        mv.setViewName("order/orderinfo/list");
        return mv;
    }

    @RequestMapping("/add.html")
    @PreAuthorize("hasAuthority('order:orderinfo:add')")
    public ModelAndView toAdd(ModelAndView mv) {
        // 查出所有企业客户
        List<TbCustomer> list = customerService.list();
        // 将企业客户信息传入
        mv.addObject("custs", list);
        mv.setViewName("order/orderinfo/add");
        return mv;
    }

    @GetMapping("/{id}.html")
    @PreAuthorize("hasAuthority('order:orderinfo:update')")
    public ModelAndView toUpdate(@PathVariable("id") String id, ModelAndView mv,HttpServletRequest request,HttpServletResponse response) {
        mv.setViewName("order/orderinfo/update");


         //发货：物流、物流单号必填、发货之后基本信息不可修改，状态更改为已发货,发货时间是当前操作时间

         //修改物流信息：物流、物流单号必填

         //确认收货：状态更改为已收货、收货时间是当前操作时间、不可进行其他操作








        mv.addObject("obj", entityService.getById(id));
        mv.addObject("id", id);
        request.getSession().setAttribute("order_id",id);
        return mv;
    }

    @RequestMapping("list")
    @PreAuthorize("hasAuthority('order:orderinfo:list')")
    public ResponseEntity page(LayuiPage layuiPage, HttpServletRequest httpServletRequest) {
        //搜索条件：如依据订单的产品名称和地址
        String parameterName = httpServletRequest.getParameter("parameterName");
        String custId = httpServletRequest.getParameter("custId");

        if ("请选择".equals(custId)){
            custId = null;
        }

        SystemCheckUtils.getInstance().checkMaxPage(layuiPage);
        IPage page = new Page<>(layuiPage.getPage(), layuiPage.getLimit());

        entityService.lambdaQuery()
                .like(!StringUtils.isEmpty(parameterName), TbOrderInfo::getAddress, parameterName)
                .or()
                .like(!StringUtils.isEmpty(parameterName), TbOrderInfo::getProdName, parameterName)
                .eq(!StringUtils.isEmpty(custId), TbOrderInfo::getCustId, custId)
                .page(page);

        List<TbOrderInfo> records = page.getRecords();
        for (TbOrderInfo record : records) {
            if (record.getStatus().equals(0)){//list.html订单状态回显地方在哪

            }
            //点击发货后，会获取当前发货订单id,并修改发货时间
            Object order_id = httpServletRequest.getSession().getAttribute("order_id");
            if(!org.springframework.util.StringUtils.isEmpty(order_id) && record.getId().equals(order_id)){
                record.setDeliverTime(LocalDateTime.now());

            }


        }
        IPage iPage = page.setRecords(records);
        return ResponseEntity.ok(LayuiTools.toLayuiTableModel(iPage));
    }

    @SameUrlData
    @PostMapping("save")
    @SysLog(value = LogModules.SAVE, module = LogModule)
    @PreAuthorize("hasAuthority('order:orderinfo:add')")
    public ResponseEntity<ApiModel> save(@Validated({AddGroup.class}) @RequestBody TbOrderInfo entity) {
        // 状态 0 未发货 1 已发货 2 已收货
        entity.setStatus(0);
        entityService.save(entity);
        return ResponseEntity.ok(ApiModel.ok());
    }

    @SameUrlData
    @SysLog(value = LogModules.UPDATE, module = LogModule)
    @PutMapping("update")
    @PreAuthorize("hasAuthority('order:orderinfo:update')")
    public ResponseEntity<ApiModel> update(@Validated({UpdateGroup.class}) @RequestBody TbOrderInfo entity) {
        entityService.updateById(entity);

        return ResponseEntity.ok(ApiModel.ok());
    }

    @SysLog(value = LogModules.DELETE, module = LogModule)
    @DeleteMapping("delete/{id}")
    @PreAuthorize("hasAuthority('order:orderinfo:delete')")
    public ResponseEntity<ApiModel> delete(@PathVariable("id") String id) {
        entityService.removeById(id);
        return ResponseEntity.ok(ApiModel.ok());
    }




}
