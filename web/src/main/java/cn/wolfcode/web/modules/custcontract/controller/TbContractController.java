package cn.wolfcode.web.modules.custcontract.controller;

import cn.wolfcode.web.modules.custinfo.entity.TbCustomer;
import cn.wolfcode.web.modules.custinfo.service.ITbCustomerService;
import cn.wolfcode.web.modules.sys.entity.SysUser;
import cn.wolfcode.web.modules.sys.form.LoginForm;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.wolfcode.web.modules.log.LogModules;

import cn.wolfcode.web.modules.custcontract.entity.TbContract;
import cn.wolfcode.web.modules.custcontract.service.ITbContractService;

import cn.wolfcode.web.commons.entity.LayuiPage;
import cn.wolfcode.web.commons.utils.LayuiTools;
import cn.wolfcode.web.commons.utils.SystemCheckUtils;
import cn.wolfcode.web.modules.BaseController;
import link.ahsj.core.annotations.AddGroup;
import link.ahsj.core.annotations.SameUrlData;
import link.ahsj.core.annotations.SysLog;
import link.ahsj.core.annotations.UpdateGroup;
import link.ahsj.core.entitys.ApiModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author xyf
 * @since 2023-06-29
 */
@Controller
@RequestMapping("custcontract")
public class TbContractController extends BaseController {

    @Autowired
    private ITbContractService entityService;

    @Autowired
    private ITbCustomerService customerService;

    private static final String LogModule = "TbContract";

    @GetMapping("/list.html")
    public ModelAndView list(ModelAndView mv) {
        // 查出所有企业客户
        List<TbContract> list = entityService.list();
        for (TbContract tbContract : list) {
            switch (tbContract.getAffixSealStatus()){
                case 0 :
                    tbContract.setAffixSealStatusString("否");
                    break;
                case 1:
                    tbContract.setAffixSealStatusString("是");
                    break;
            }

            switch (tbContract.getAuditStatus()){
                case 0 :
                    tbContract.setAuditStatusString("未审核");
                    break;
                case 1:
                    tbContract.setAuditStatusString("通过审核");
                    break;
                case -1:
                    tbContract.setAuditStatusString("审核不通过");
                    break;
            }

            switch (tbContract.getNullifyStatus()){
                case 0 :
                    tbContract.setNullifyStatusString("在用");
                    break;
                case 1:
                    tbContract.setNullifyStatusString("废用");
                    break;
            }

        }
        // 将企业客户信息传入
        mv.addObject("contracts",list);
        mv.setViewName("contract/custcontract/list");
        return mv;
    }

    @RequestMapping("/add.html")
    @PreAuthorize("hasAuthority('contract:custcontract:add')")
    public ModelAndView toAdd(ModelAndView mv) {
        // 查出所有企业客户
        List<TbCustomer> list = customerService.list();
        // 将企业客户信息传入
        mv.addObject("custs",list);
        mv.setViewName("contract/custcontract/add");
        return mv;
    }

    @GetMapping("/{id}.html")
    @PreAuthorize("hasAuthority('contract:custcontract:update')")
    public ModelAndView toUpdate(@PathVariable("id") String id, ModelAndView mv) {
        mv.setViewName("contract/custcontract/update");
        mv.addObject("obj", entityService.getById(id));
        mv.addObject("id", id);
        return mv;
    }

    @RequestMapping("list")
    @PreAuthorize("hasAuthority('contract:custcontract:list')")
    public ResponseEntity page(LayuiPage layuiPage,String parameterName,String nullifyStatus,String affixSealStatus,String auditStatus) {
        SystemCheckUtils.getInstance().checkMaxPage(layuiPage);
        IPage page = new Page<>(layuiPage.getPage(), layuiPage.getLimit());

        if("请选择".equals(nullifyStatus) && "请选择".equals(affixSealStatus) &&"请选择".equals(auditStatus)){
            page = entityService.lambdaQuery()
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractCode, parameterName)
                    .or()
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractName, parameterName)
                    .page(page);
        }else if("请选择".equals(nullifyStatus) && "请选择".equals(affixSealStatus) && !"请选择".equals(auditStatus)){
            page = entityService.lambdaQuery()
                    .eq(!StringUtils.isEmpty(auditStatus),TbContract::getAuditStatus, auditStatus)
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractCode, parameterName)
                    .or()
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractCode, parameterName)
                    .page(page);
        }else if(!"请选择".equals(nullifyStatus) && "请选择".equals(affixSealStatus) && "请选择".equals(auditStatus)){
            page = entityService.lambdaQuery()
                    .eq(!StringUtils.isEmpty(nullifyStatus),TbContract::getNullifyStatus, nullifyStatus)
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractCode, parameterName)
                    .or()
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractCode, parameterName)
                    .page(page);
        } else if ("请选择".equals(nullifyStatus) && !"请选择".equals(affixSealStatus) && "请选择".equals(auditStatus)) {
            page = entityService.lambdaQuery()
                    .eq(!StringUtils.isEmpty(affixSealStatus),TbContract::getAffixSealStatus, affixSealStatus)
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractCode, parameterName)
                    .or()
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractCode, parameterName)
                    .page(page);
        }else if(!"请选择".equals(nullifyStatus) && !"请选择".equals(affixSealStatus) &&"请选择".equals(auditStatus)){
            page = entityService.lambdaQuery()
                    .eq(!StringUtils.isEmpty(affixSealStatus),TbContract::getAffixSealStatus, affixSealStatus)
                    .eq(!StringUtils.isEmpty(nullifyStatus),TbContract::getNullifyStatus, nullifyStatus)
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractCode, parameterName)
                    .or()
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractCode, parameterName)
                    .page(page);
        }else if(!"请选择".equals(nullifyStatus) && "请选择".equals(affixSealStatus) &&!"请选择".equals(auditStatus)){
            page = entityService.lambdaQuery()
                    .eq(!StringUtils.isEmpty(auditStatus),TbContract::getAuditStatus, auditStatus)
                    .eq(!StringUtils.isEmpty(nullifyStatus),TbContract::getNullifyStatus, nullifyStatus)
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractCode, parameterName)
                    .or()
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractCode, parameterName)
                    .page(page);
        }else if ("请选择".equals(nullifyStatus) && !"请选择".equals(affixSealStatus) && !"请选择".equals(auditStatus)){
            page = entityService.lambdaQuery()
                    .eq(!StringUtils.isEmpty(affixSealStatus),TbContract::getAffixSealStatus, affixSealStatus)
                    .eq(!StringUtils.isEmpty(auditStatus),TbContract::getAuditStatus, auditStatus)
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractCode, parameterName)
                    .or()
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractCode, parameterName)
                    .page(page);
        }else{
            page = entityService.lambdaQuery()
                    .eq(!StringUtils.isEmpty(affixSealStatus),TbContract::getAffixSealStatus, affixSealStatus)
                    .eq(!StringUtils.isEmpty(auditStatus),TbContract::getAuditStatus, auditStatus)
                    .eq(!StringUtils.isEmpty(nullifyStatus),TbContract::getNullifyStatus, nullifyStatus)
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractCode, parameterName)
                    .or()
                    .like(!StringUtils.isEmpty(parameterName),TbContract::getContractCode, parameterName)
                    .page(page);
        }
        return ResponseEntity.ok(LayuiTools.toLayuiTableModel(page));
    }

    @SameUrlData
    @PostMapping("save")
    @SysLog(value = LogModules.SAVE, module =LogModule)
    @PreAuthorize("hasAuthority('contract:custcontract:add')")
    public ResponseEntity<ApiModel> save(@Validated({AddGroup.class}) @RequestBody TbContract entity, HttpServletRequest request) {
        // 录入当前系统时间
        entity.setInputTime(LocalDateTime.now());
        // 录入人填写
        SysUser loginUser =(SysUser) request.getSession().getAttribute(LoginForm.LOGIN_USER_KEY);
        entity.setInputUser(loginUser.getUsername());
        entityService.save(entity);
        return ResponseEntity.ok(ApiModel.ok());
    }

    @SameUrlData
    @SysLog(value = LogModules.UPDATE, module = LogModule)
    @PutMapping("update")
    @PreAuthorize("hasAuthority('contract:custcontract:update')")
    public ResponseEntity<ApiModel> update(@Validated({UpdateGroup.class}) @RequestBody TbContract entity) {
        entityService.updateById(entity);
        return ResponseEntity.ok(ApiModel.ok());
    }

    @SysLog(value = LogModules.DELETE, module = LogModule)
    @DeleteMapping("delete/{id}")
    @PreAuthorize("hasAuthority('contract:custcontract:delete')")
    public ResponseEntity<ApiModel> delete(@PathVariable("id") String id) {
        entityService.removeById(id);
        return ResponseEntity.ok(ApiModel.ok());
    }

}
