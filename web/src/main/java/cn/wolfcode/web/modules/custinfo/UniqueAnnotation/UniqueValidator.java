package cn.wolfcode.web.modules.custinfo.UniqueAnnotation;

import cn.wolfcode.web.modules.custinfo.entity.TbCustomer;
import cn.wolfcode.web.modules.custinfo.service.ITbCustomerService;
import freemarker.cache.ConcurrentCacheStorage;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueValidator implements ConstraintValidator<Unique,String> {
    @Autowired
    private ITbCustomerService entityService;

    @Override
    public void initialize(Unique constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String inputCustomerName, ConstraintValidatorContext constraintValidatorContext) {
        if(entityService.lambdaQuery().eq(TbCustomer::getCustomerName,inputCustomerName).count()!=0){
            return false;
        }
        return true;
    }
}
