package cn.wolfcode.web.modules.appdemo.service.impl;

import cn.wolfcode.web.modules.appdemo.entity.Appdemo;
import cn.wolfcode.web.modules.appdemo.mapper.AppdemoMapper;
import cn.wolfcode.web.modules.appdemo.service.IAppdemoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xyf
 * @since 2023-06-25
 */
@Service
public class AppdemoServiceImpl extends ServiceImpl<AppdemoMapper, Appdemo> implements IAppdemoService {

}
