package cn.wolfcode.web.modules.custcontract.service;

import cn.wolfcode.web.modules.custcontract.entity.TbContract;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 合同信息 服务类
 * </p>
 *
 * @author xyf
 * @since 2023-06-29
 */
public interface ITbContractService extends IService<TbContract> {

}
