package cn.wolfcode.web.modules.custcontract.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import link.ahsj.core.annotations.AddGroup;
import link.ahsj.core.annotations.UpdateGroup;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 合同信息
 * </p>
 *
 * @author xyf
 * @since 2023-06-29
 */
public class TbContract implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 客户id
     */
    private String custId;

    /**
     * 合同名称
     */
    @NotBlank(message="请填写合同名称",groups = {AddGroup.class, UpdateGroup.class})
    @Length(max=100,message = "合同名称不超过100字",groups = {AddGroup.class, UpdateGroup.class})
    private String contractName;

    /**
     * 合同编码
     */
    @NotBlank(message="请填写合同编码",groups = {AddGroup.class, UpdateGroup.class})
    @Length(max=50,message = "合同编码不超过50字",groups = {AddGroup.class, UpdateGroup.class})
    private String contractCode;

    /**
     * 合同金额
     */
    @NotNull(message = "请填写合同金额",groups = {AddGroup.class, UpdateGroup.class})
    @Range(message = "合同金额需为正数",min = 1)
    private Integer amounts;

    /**
     * 合同生效开始时间
     */
    @NotNull(message = "请选择合同生效开始时间",groups = {AddGroup.class, UpdateGroup.class})
    private LocalDate startDate;

    /**
     * 合同生效结束时间
     */
    @NotNull(message = "请选择合同生效结束时间",groups = {AddGroup.class, UpdateGroup.class})
    private LocalDate endDate;

    /**
     * 合同内容
     */
    @NotBlank(message="请填写合同内容",groups = {AddGroup.class, UpdateGroup.class})
    private String content;

    /**
     * 是否盖章确认 0 否 1 是
     */
    private Integer affixSealStatus;

    @TableField(exist = false)
    private String affixSealStatusString;

    /**
     * 审核状态 0 未审核 1 审核通过 -1 审核不通过
     */
    private Integer auditStatus;

    @TableField(exist = false)
    private String auditStatusString;

    /**
     * 是否作废 1 作废 0 在用
     */
    private Integer nullifyStatus;

    @TableField(exist = false)
    private String nullifyStatusString;

    /**
     * 录入人
     */
    private String inputUser;

    /**
     * 录入时间
     */
    private LocalDateTime inputTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;


    public String getAffixSealStatusString() {
        return affixSealStatusString;
    }

    public void setAffixSealStatusString(String affixSealStatusString) {
        this.affixSealStatusString = affixSealStatusString;
    }

    public String getAuditStatusString() {
        return auditStatusString;
    }

    public void setAuditStatusString(String auditStatusString) {
        this.auditStatusString = auditStatusString;
    }

    public String getNullifyStatusString() {
        return nullifyStatusString;
    }

    public void setNullifyStatusString(String nullifyStatusString) {
        this.nullifyStatusString = nullifyStatusString;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }
    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }
    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }
    public Integer getAmounts() {
        return amounts;
    }

    public void setAmounts(Integer amounts) {
        this.amounts = amounts;
    }
    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }
    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public Integer getAffixSealStatus() {
        return affixSealStatus;
    }

    public void setAffixSealStatus(Integer affixSealStatus) {
        this.affixSealStatus = affixSealStatus;
    }
    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }
    public Integer getNullifyStatus() {
        return nullifyStatus;
    }

    public void setNullifyStatus(Integer nullifyStatus) {
        this.nullifyStatus = nullifyStatus;
    }
    public String getInputUser() {
        return inputUser;
    }

    public void setInputUser(String inputUser) {
        this.inputUser = inputUser;
    }
    public LocalDateTime getInputTime() {
        return inputTime;
    }

    public void setInputTime(LocalDateTime inputTime) {
        this.inputTime = inputTime;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "TbContract{" +
            "id=" + id +
            ", custId=" + custId +
            ", contractName=" + contractName +
            ", contractCode=" + contractCode +
            ", amounts=" + amounts +
            ", startDate=" + startDate +
            ", endDate=" + endDate +
            ", content=" + content +
            ", affixSealStatus=" + affixSealStatus +
            ", auditStatus=" + auditStatus +
            ", nullifyStatus=" + nullifyStatus +
            ", inputUser=" + inputUser +
            ", inputTime=" + inputTime +
            ", updateTime=" + updateTime +
        "}";
    }
}
