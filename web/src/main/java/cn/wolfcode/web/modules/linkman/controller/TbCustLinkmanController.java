package cn.wolfcode.web.modules.linkman.controller;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.wolfcode.web.commons.utils.PoiExportHelper;
import cn.wolfcode.web.modules.custinfo.entity.TbCustomer;
import cn.wolfcode.web.modules.custinfo.service.ITbCustomerService;
import cn.wolfcode.web.modules.sys.entity.SysUser;
import cn.wolfcode.web.modules.sys.form.LoginForm;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.wolfcode.web.modules.log.LogModules;

import cn.wolfcode.web.modules.linkman.entity.TbCustLinkman;
import cn.wolfcode.web.modules.linkman.service.ITbCustLinkmanService;

import cn.wolfcode.web.commons.entity.LayuiPage;
import cn.wolfcode.web.commons.utils.LayuiTools;
import cn.wolfcode.web.commons.utils.SystemCheckUtils;
import cn.wolfcode.web.modules.BaseController;
import link.ahsj.core.annotations.AddGroup;
import link.ahsj.core.annotations.SameUrlData;
import link.ahsj.core.annotations.SysLog;
import link.ahsj.core.annotations.UpdateGroup;
import link.ahsj.core.entitys.ApiModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author xyf
 * @since 2023-06-27
 */
@Controller
@RequestMapping("linkman")
public class TbCustLinkmanController extends BaseController {

    @Autowired
    private ITbCustLinkmanService entityService;

    @Autowired
    private ITbCustomerService customerService;



    private static final String LogModule = "TbCustLinkman";

    @GetMapping("/list.html")
    public ModelAndView list(ModelAndView mv) {
        // 查出所有企业客户
        List<TbCustomer> list = customerService.list();
        // 将企业客户信息传入
        mv.addObject("custs",list);
        mv.setViewName("user/linkman/list");
        return mv;
    }

    @RequestMapping("/add.html")
    @PreAuthorize("hasAuthority('user:linkman:add')")
    public ModelAndView toAdd(ModelAndView mv) {
        // 查出所有企业客户
        List<TbCustomer> list = customerService.list();
        // 将企业客户信息传入
        mv.addObject("custs",list);
        mv.setViewName("user/linkman/add");
        return mv;
    }

    @GetMapping("/{id}.html")
    @PreAuthorize("hasAuthority('user:linkman:update')")
    public ModelAndView toUpdate(@PathVariable("id") String id, ModelAndView mv) {
        mv.setViewName("user/linkman/update");
        mv.addObject("obj", entityService.getById(id));
        mv.addObject("id", id);
        return mv;
    }

    @RequestMapping("list")
    @PreAuthorize("hasAuthority('user:linkman:list')")
    public ResponseEntity page(LayuiPage layuiPage,String parameterName,String custId) {
        SystemCheckUtils.getInstance().checkMaxPage(layuiPage);
        IPage page = new Page<>(layuiPage.getPage(), layuiPage.getLimit());

        if("请选择".equals(custId)){
            page = entityService.lambdaQuery()
                    .like(!StringUtils.isEmpty(parameterName),TbCustLinkman::getLinkman,parameterName)// 按照联系人搜索
                    .or()
                    .like(!StringUtils.isEmpty(parameterName),TbCustLinkman::getPhone,parameterName)// 按照电话搜索
                    .page(page);
        }else {
            page = entityService.lambdaQuery()
                    .eq(!StringUtils.isEmpty(custId),TbCustLinkman::getCustId,custId)
                    .like(!StringUtils.isEmpty(parameterName),TbCustLinkman::getLinkman,parameterName)// 按照联系人搜索
                    .or()
                    .like(!StringUtils.isEmpty(parameterName),TbCustLinkman::getPhone,parameterName)// 按照电话搜索
                    .page(page);
        }


        return ResponseEntity.ok(LayuiTools.toLayuiTableModel(page));
    }

    @SameUrlData
    @PostMapping("save")
    @SysLog(value = LogModules.SAVE, module =LogModule)
    @PreAuthorize("hasAuthority('user:linkman:add')")
    public ResponseEntity<ApiModel> save(@Validated({AddGroup.class}) @RequestBody TbCustLinkman entity, HttpServletRequest request) {
        // 录入当前系统时间
        entity.setInputTime(LocalDateTime.now());
        // 录入人填写
        SysUser loginUser =(SysUser) request.getSession().getAttribute(LoginForm.LOGIN_USER_KEY);
        entity.setInputUser(loginUser.getUsername());
        entityService.save(entity);
        return ResponseEntity.ok(ApiModel.ok());
    }

    @SameUrlData
    @SysLog(value = LogModules.UPDATE, module = LogModule)
    @PutMapping("update")
    @PreAuthorize("hasAuthority('user:linkman:update')")
    public ResponseEntity<ApiModel> update(@Validated({UpdateGroup.class}) @RequestBody TbCustLinkman entity) {
        System.out.println("任职状态" + entity.getStatus());
        entityService.updateById(entity);
        return ResponseEntity.ok(ApiModel.ok());
    }

    @SysLog(value = LogModules.DELETE, module = LogModule)
    @DeleteMapping("delete/{id}")
    @PreAuthorize("hasAuthority('user:linkman:delete')")
    public ResponseEntity<ApiModel> delete(@PathVariable("id") String id) {
        entityService.removeById(id);
        return ResponseEntity.ok(ApiModel.ok());
    }

    /**
     * 导出
     */
    @SysLog(value = LogModules.DELETE,module = LogModule)
    @RequestMapping("export")
    public void export(String parameterName, String custId, HttpServletResponse response){
        // 把数据导出到表格当中
        List<TbCustLinkman> list = entityService.lambdaQuery()
                .eq(!StringUtils.isEmpty(custId), TbCustLinkman::getCustId, custId)
                .like(!StringUtils.isEmpty(parameterName), TbCustLinkman::getLinkman, parameterName)// 按照联系人搜索
                .or()
                .like(!StringUtils.isEmpty(parameterName), TbCustLinkman::getPhone, parameterName)// 按照电话搜索
                .list();

        // 执行文件导出 准备工作
        ExportParams exportParams = new ExportParams();
        /**
         * 参数一：样式
         * 参数二：导出的实体类的字节码
         * 参数三：数据列表
         */
        Workbook workbook = ExcelExportUtil.exportExcel(exportParams, TbCustLinkman.class, list);

        // 导出
        try {
            PoiExportHelper.exportExcel(response,"联系人管理\n"+LocalDateTime.now(),workbook);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }


    @RequestMapping("listByCustomerId")
    public ResponseEntity<ApiModel> listByCustomerId(String custId){
//        System.out.println("---------------"+custId);

        if(StringUtils.isEmpty(custId)){
            return ResponseEntity.ok(ApiModel.ok());
        }

        List<TbCustLinkman> list = entityService.lambdaQuery().eq(!StringUtils.isEmpty(custId), TbCustLinkman::getCustId, custId).list();

        return ResponseEntity.ok(ApiModel.data(list));
    }

}
