package cn.wolfcode.web.modules.custinfo.service;

import cn.wolfcode.web.modules.custinfo.entity.TbCustomer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 客户信息 服务类
 * </p>
 *
 * @author xyf
 * @since 2023-06-26
 */
public interface ITbCustomerService  extends IService<TbCustomer> {

}
