package cn.wolfcode.web.modules.appdemo.mapper;

import cn.wolfcode.web.modules.appdemo.entity.Appdemo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xyf
 * @since 2023-06-25
 */
public interface AppdemoMapper extends BaseMapper<Appdemo> {

}
