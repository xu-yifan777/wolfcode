package cn.wolfcode.web.modules.custinfo.mapper;

import cn.wolfcode.web.modules.custinfo.entity.TbCustomer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客户信息 Mapper 接口
 * </p>
 *
 * @author xyf
 * @since 2023-06-26
 */
public interface TbCustomerMapper extends BaseMapper<TbCustomer> {

}
