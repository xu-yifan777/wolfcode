package cn.wolfcode.web.modules.linkman.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import link.ahsj.core.annotations.AddGroup;
import link.ahsj.core.annotations.UpdateGroup;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 客户联系人
 * </p>
 *
 * @author xyf
 * @since 2023-06-27
 */
public class TbCustLinkman implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 客户id
     */
    @NotBlank(message="请选择客户",groups = {AddGroup.class, UpdateGroup.class})
    @Excel(name = "所属客户")
    private String custId;

    /**
     * 联系人名字
     */
    @NotBlank(message="请填写联系人名字",groups = {AddGroup.class, UpdateGroup.class})
    @Length(max=30,message = "客户不超过30字",groups = {AddGroup.class, UpdateGroup.class})
    @Excel(name = "联系人姓名")
    private String linkman;

    /**
     * 性别 1 男 0 女
     */
    @Excel(name = "性别")
    private Integer sex;

    /**
     * 年龄
     */
    @Range(min = 1,max = 100,message = "年龄为不大于100的自然数",groups = {AddGroup.class, UpdateGroup.class})
    @Excel(name = "年龄")
    private Integer age;

    /**
     * 联系人电话
     */
    @NotBlank(message="请填写联系人电话",groups = {AddGroup.class, UpdateGroup.class})
//    @DecimalMin(value = "0",groups = {AddGroup.class, UpdateGroup.class})
//    @DecimalMax(value = "20",message = "电话号码不可超过20位数",groups = {AddGroup.class, UpdateGroup.class})
    @Length(max=20,message = "电话号码不超过20字",groups = {AddGroup.class, UpdateGroup.class})
    @Excel(name = "电话号码")
    private String phone;

    /**
     * 职位
     */
    @NotBlank(message="请填写职位",groups = {AddGroup.class, UpdateGroup.class})
    @Length(max=20,message = "职位不超过20字",groups = {AddGroup.class, UpdateGroup.class})
    @Excel(name = "职位")
    private String position;

    /**
     * 部门
     */
    @NotBlank(message="请填写部门",groups = {AddGroup.class, UpdateGroup.class})
    @Length(max=20,message = "部门不超过20字",groups = {AddGroup.class, UpdateGroup.class})
    private String department;

    /**
     * 任职状态 在职0 离职1
     */

    private Integer status;

    /**
     * 备注信息
     */
    private String remark;

    /**
     * 录入人
     */
    @Excel(name = "录入人")
    private String inputUser;

    /**
     * 录入时间
     */
    private LocalDateTime inputTime;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }
    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }
    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getInputUser() {
        return inputUser;
    }

    public void setInputUser(String inputUser) {
        this.inputUser = inputUser;
    }
    public LocalDateTime getInputTime() {
        return inputTime;
    }

    public void setInputTime(LocalDateTime inputTime) {
        this.inputTime = inputTime;
    }

    @Override
    public String toString() {
        return "TbCustLinkman{" +
            "id=" + id +
            ", custId=" + custId +
            ", linkman=" + linkman +
            ", sex=" + sex +
            ", age=" + age +
            ", phone=" + phone +
            ", position=" + position +
            ", department=" + department +
            ", remark=" + remark +
            ", inputUser=" + inputUser +
            ", inputTime=" + inputTime +
        "}";
    }
}
