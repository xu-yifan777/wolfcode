package cn.wolfcode.web.modules.custcontract.service.impl;

import cn.wolfcode.web.modules.custcontract.entity.TbContract;
import cn.wolfcode.web.modules.custcontract.mapper.TbContractMapper;
import cn.wolfcode.web.modules.custcontract.service.ITbContractService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 合同信息 服务实现类
 * </p>
 *
 * @author xyf
 * @since 2023-06-29
 */
@Service
public class TbContractServiceImpl extends ServiceImpl<TbContractMapper, TbContract> implements ITbContractService {

}
