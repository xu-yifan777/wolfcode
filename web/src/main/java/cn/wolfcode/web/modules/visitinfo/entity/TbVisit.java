package cn.wolfcode.web.modules.visitinfo.entity;

import link.ahsj.core.annotations.AddGroup;
import link.ahsj.core.annotations.UpdateGroup;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 拜访信息表
 * </p>
 *
 * @author xyf
 * @since 2023-06-29
 */
public class TbVisit implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 唯一id
     */
    private String id;

    /**
     * 所属企业
     */
    @NotBlank(message="请选择所属企业",groups = {AddGroup.class, UpdateGroup.class})
    private String custId;

    /**
     * 联系人id
     */
    @NotNull(message="请选择联系人",groups = {AddGroup.class, UpdateGroup.class})
    private String linkmanId;

    /**
     * 拜访方式, 1 上门走访, 2 电话拜访
     */
    @NotNull(message="请选择拜访方式",groups = {AddGroup.class, UpdateGroup.class})
    private Integer visitType;

    /**
     * 拜访原因
     */
    @NotBlank(message="请填写拜访原因",groups = {AddGroup.class, UpdateGroup.class})
    @Length(max=100,message = "拜访原因不超过100字",groups = {AddGroup.class, UpdateGroup.class})
    private String visitReason;

    /**
     * 交流内容
     */
    @NotBlank(message="请填写交流内容",groups = {AddGroup.class, UpdateGroup.class})
    @Length(max=1000,message = "交流情况不超过1000字",groups = {AddGroup.class, UpdateGroup.class})
    private String content;

    /**
     * 拜访时间
     */
    @Past(message = "拜访时间不可超过当天",groups = {AddGroup.class, UpdateGroup.class})
    private LocalDate visitDate;

    /**
     * 录入人
     */
    private String inputUser;

    /**
     * 录入时间
     */
    private LocalDateTime inputTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }
    public String getLinkmanId() {
        return linkmanId;
    }

    public void setLinkmanId(String linkmanId) {
        this.linkmanId = linkmanId;
    }
    public Integer getVisitType() {
        return visitType;
    }

    public void setVisitType(Integer visitType) {
        this.visitType = visitType;
    }
    public String getVisitReason() {
        return visitReason;
    }

    public void setVisitReason(String visitReason) {
        this.visitReason = visitReason;
    }
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public LocalDate getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(LocalDate visitDate) {
        this.visitDate = visitDate;
    }
    public String getInputUser() {
        return inputUser;
    }

    public void setInputUser(String inputUser) {
        this.inputUser = inputUser;
    }
    public LocalDateTime getInputTime() {
        return inputTime;
    }

    public void setInputTime(LocalDateTime inputTime) {
        this.inputTime = inputTime;
    }

    @Override
    public String toString() {
        return "TbVisit{" +
            "id=" + id +
            ", custId=" + custId +
            ", linkmanId=" + linkmanId +
            ", visitType=" + visitType +
            ", visitReason=" + visitReason +
            ", content=" + content +
            ", visitDate=" + visitDate +
            ", inputUser=" + inputUser +
            ", inputTime=" + inputTime +
        "}";
    }
}
